package com.example.piotrek.bikespeedometer;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by ptr on 2015-03-31.
 */
public class StarterAsyncTask extends AsyncTask<String, String, BluetoothSocket> {
    private BluetoothSocket socket;
    private BluetoothDevice speedometerDevice;
    private BluetoothAdapter BA;

    public StarterAsyncTask (BluetoothDevice device) {
        speedometerDevice = device;
        BA = BluetoothAdapter.getDefaultAdapter();
        BluetoothSocket tmp = null;
        try {
            UUID newUUID = speedometerDevice.getUuids()[0].getUuid();
            tmp = speedometerDevice.createRfcommSocketToServiceRecord(newUUID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        socket = tmp;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected BluetoothSocket doInBackground(String... params) {
        try {
            BA.cancelDiscovery();
        } catch (NullPointerException exc) {
            Log.d(Constants.TAG, "Null pointer exception while trying to cancel disvocery before starting the connection.");
        }
        try {
            socket.connect();
        } catch (IOException connectException) {
            try {
                socket.close();
            } catch (IOException closeException) {
                Log.d(Constants.TAG, "IO exception while trying to close socket after bad connection start.");
            }
        }
        return socket;
    }

//    @Override
//    protected void onPostExecute(String s) {
//        super.onPostExecute(s);
//    }
}
