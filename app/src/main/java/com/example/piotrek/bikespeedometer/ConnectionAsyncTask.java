package com.example.piotrek.bikespeedometer;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.util.ByteArrayBuffer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Created by ptr on 2015-03-31.
 */
public class ConnectionAsyncTask extends AsyncTask<String, String, String>{
    private BluetoothSocket socket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    private StringBuilder bufferString;
    private char[] bufferCharArray;
    private int messageCount =0;

    public ConnectionAsyncTask(BluetoothSocket socket) {
        this.socket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
        }
        mmInStream = tmpIn;
        mmOutStream = tmpOut;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        byte[] buffer = new byte[6];
        StringBuilder dataString = new StringBuilder();
        int bytes;
        char a;
        try {
            bytes = mmInStream.read(buffer);
            if (bytes != -1) {
//                bufferCharArray = new char[buffer.length * 2];
//                for (int i = 0; i < buffer.length; i++) {
//                    bufferCharArray[i * 2] = "0123456789ABCDEF".charAt((buffer[i] >> 4) & 15);
//                    bufferCharArray[i * 2 + 1] = "0123456789ABCDEF".charAt(buffer[i] & 15);
//                }
                for (int i=0; i<buffer.length; i++) {
                    dataString.append((char)buffer[i]);
                }
//                Intent intent = new Intent();
//                intent.setAction(Constants.ACTION_SPEED_DATA_RECEIVED);
//                intent.putExtra(Constants.EXTRA_SPEED_DATA, bufferString);
//                intent.putExtra(Constants.EXTRA_MESSAGE_COUNT, messageCount);
//                this.sendBroadcast(intent);
            }
        } catch (IOException e) {
            Log.d(Constants.TAG, "IO exception while reading bluetooth data");
        }
        return dataString.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
