package com.example.piotrek.bikespeedometer;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends ActionBarActivity {
    private static String speedometerMAC = "98:D3:31:80:15:9A";
    private String deviceFoundMAC;
    private boolean deviceFound = false;
    private BluetoothDevice bikeSpeedometer;
    private BluetoothAdapter BA;
    private Button connectButton, disconnectButton, configButton;
    private TextView titleText, authorText;
    private Timer timer;
    private TimerTask timerTask;
    private final Handler handler = new Handler();
    private Intent startConnectionServiceIntent;

    private final BroadcastReceiver actionFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action) && !deviceFound) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getAddress().equals(speedometerMAC)) {
                    Log.d(Constants.TAG, "Found Bike Speedometer device! Address: " + speedometerMAC);
                    bikeSpeedometer = device;
                    deviceFound = true;
                    BA.cancelDiscovery();
                    disconnectButton.setVisibility(View.VISIBLE);
                    connectButton.setClickable(false);

                    Log.d(Constants.TAG,"Starting ConnectionService");
                    startConnectionServiceIntent = new Intent(MainActivity.this, ConnectionService.class);
                    startConnectionServiceIntent.putExtra(Constants.EXTRA_SPEEDOMETER_DEVICE, bikeSpeedometer);
                  //  startConnectionServiceIntent.putExtra(Constants.EXTRA_TEXTVIEW_TO_UPDATE, )
                    startService(startConnectionServiceIntent);

//                    Log.d(Constants.TAG,"Starting ConnectionActivity");
//                    Intent startConnectionActivityIntent = new Intent(MainActivity.this, ConnectionActivity.class);
//                    startConnectionActivityIntent.putExtra(Constants.EXTRA_SPEEDOMETER_DEVICE, bikeSpeedometer);
//                    startConnectionActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(startConnectionActivityIntent);
                }
                else {
                    deviceFound = false;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        connectButton = (Button) findViewById(R.id.connectButton);
        disconnectButton = (Button) findViewById(R.id.disconnectButton);
        disconnectButton.setVisibility(View.INVISIBLE);
        configButton = (Button) findViewById(R.id.configButton);
        titleText = (TextView) findViewById(R.id.titleText);
        authorText = (TextView) findViewById(R.id.authorText);
        registerReceiver(actionFoundReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));


        BA = BluetoothAdapter.getDefaultAdapter();
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intent, 0);



        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(Constants.TAG, "CONNECT button clicked. Starting discovery for Bluetooth devices");
                BA.startDiscovery();
                disconnectButton.setVisibility(View.VISIBLE);
                connectButton.setClickable(false);
                Log.d(Constants.TAG,"Starting ConnectionActivity");
                Intent startConnectionActivityIntent = new Intent(MainActivity.this, ConnectionActivity.class);
                startConnectionActivityIntent.putExtra(Constants.EXTRA_SPEEDOMETER_DEVICE, bikeSpeedometer);
                //startConnectionActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startConnectionActivityIntent);
            }
        });

        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(Constants.TAG, "DISCONNECT button clicked. Stopping ConnectionService");
                stopService(startConnectionServiceIntent);
                disconnectButton.setVisibility(View.INVISIBLE);
                connectButton.setClickable(true);
            }
        });

        configButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(Constants.TAG, "CONFIGURE button clicked. Starting ConfigActivity");
                Intent intent = new Intent(MainActivity.this, ConfigActivity.class);
                getApplicationContext().startActivity(intent);
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(actionFoundReceiver);
    }

}
