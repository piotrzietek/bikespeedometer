package com.example.piotrek.bikespeedometer;

import android.app.ProgressDialog;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Telephony;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class ConnectionService extends Service {
    private boolean deviceFound = false;
    private BluetoothDevice bikeSpeedometer;
    private BluetoothAdapter BA;
    //private ConnectThread connectThread;
    //private ConnectedThread connectedThread;
    private BluetoothSocket socket;
    private int messageCount = 0;
    private String data;

    Timer timer;
    TimerTask timerTask;

    final Handler handler = new Handler();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        BA = BluetoothAdapter.getDefaultAdapter();
        try {
            bikeSpeedometer = intent.getParcelableExtra(Constants.EXTRA_SPEEDOMETER_DEVICE);
        } catch (NullPointerException e) {
            Log.e(Constants.TAG, "null pointer in service");
        }
        deviceFound = true;
        Log.d(Constants.TAG, "ConnectionService started, device taken from extras");

        StarterAsyncTask starterTask = new StarterAsyncTask(bikeSpeedometer);
        starterTask.execute();
        try {
            socket = starterTask.get();
        } catch (InterruptedException e) {
            Log.e(Constants.TAG, "InterruptedException while getting socket from StarterAsyncTask");
        } catch (ExecutionException e) {
            Log.e(Constants.TAG, "ExecutionException while getting socket from StarterAsyncTask");
        }
        if (socket.isConnected()) {
            Intent speedometerFoundIntent = new Intent();
            speedometerFoundIntent.setAction(Constants.ACTION_SPEEDOMETER_FOUND);
            sendBroadcast(speedometerFoundIntent);
            startTimer();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 100, 1000);
    }

    public void stopTimerTask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {

                        ConnectionAsyncTask task = new ConnectionAsyncTask(socket);
                        task.execute();
                        try {
                            data = task.get();
                        } catch (Exception e) {
                            Log.d(Constants.TAG, "Exception when getting data from asynctask");
                        }
                        if (data.contains("!")) {
                            messageCount++;
                        }
                        Constants.dataTextView.setText(data);
                        if (!(Constants.countTextView.getText().equals(String.valueOf(messageCount)))) {
                            Constants.countTextView.setText(String.valueOf(messageCount));
                        }
//                        Intent intent = new Intent();
//                        intent.setAction(Constants.ACTION_SPEED_DATA_RECEIVED);
//                        intent.putExtra(Constants.EXTRA_SPEED_DATA, data);
//                        intent.putExtra(Constants.EXTRA_MESSAGE_COUNT, messageCount);
//                        sendBroadcast(intent);

                    }
                });
            }
        };
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //if (connectThread != null)         connectThread.cancel();
        stopTimerTask();
        Intent speedometerDisconnectedIntent = new Intent();
        speedometerDisconnectedIntent.setAction(Constants.ACTION_SPEEDOMETER_DISCONNECTED);
        sendBroadcast(speedometerDisconnectedIntent);
        Log.d(Constants.TAG, "ConnectionService stopped");
    }

}



/*
    public class ConnectThread extends Thread {
        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            try {
                UUID newUUID = device.getUuids()[0].getUuid();
                tmp = device.createRfcommSocketToServiceRecord(newUUID);
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "rfcomm", Toast.LENGTH_SHORT).show();
            }
            socket = tmp;
        }

        public void run() {
            try {
                BA.cancelDiscovery();
            } catch (NullPointerException exc) {
                Toast.makeText(getApplicationContext(), "cancel discovery", Toast.LENGTH_SHORT).show();
            }
            try {
                socket.connect();
            } catch (IOException connectException) {
                try {
                    socket.close();
                } catch (IOException closeException) {
                    Toast.makeText(getApplicationContext(), "close exception", Toast.LENGTH_SHORT).show();
                }
                return;
            }


            ConnectionAsyncTask task = new ConnectionAsyncTask(socket);
            task.execute();
            try {
                data = task.get().toString();
            } catch (Exception e) {
                Log.d(Constants.TAG, "Exception when getting data from asynctask");
            }
        }

        public void cancel() {
            try {
                connectedThread.cancel();
                socket.close();
            } catch (IOException e) {
            }
        }
    }

    public class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private String bufferString;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[64];
            int bytes;
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    if (bytes != -1) {
                        bufferString = buffer.toString();
                        messageCount++;
                        Intent intent = new Intent();
                        intent.setAction(Constants.ACTION_SPEED_DATA_RECEIVED);
                        intent.putExtra(Constants.EXTRA_SPEED_DATA, bufferString);
                        intent.putExtra(Constants.EXTRA_MESSAGE_COUNT, messageCount);
                        sendBroadcast(intent);
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
            }
        }
    } */
