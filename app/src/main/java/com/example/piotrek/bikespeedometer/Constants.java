package com.example.piotrek.bikespeedometer;

import android.widget.TextView;

/**
 * Created by ptr on 2015-03-23.
 */
public class Constants {
    public static final String TAG = "com.example.piotrek.bikespeedometer";
    public static final String ACTION_SPEEDOMETER_FOUND = "com.example.piotrek.bikespeedometer.ACTION_SPEEDOMETER_FOUND";
    public static final String ACTION_SPEED_DATA_RECEIVED = "com.example.piotrek.bikespeedometer.ACTION_SPEED_DATA_RECEIVED";
    public static final String ACTION_SPEEDOMETER_DISCONNECTED = "com.example.piotrek.bikespeedometer.ACTION_SPEEDOMETER_DISCONNECTED";
    public static final String EXTRA_SPEED_DATA = "com.example.piotrek.bikespeedometer.EXTRA_SPEED_DATA";
    public static final String EXTRA_SPEEDOMETER_DEVICE = "com.example.piotrek.bikespeedometer.EXTRA_SPEEDOMETER_DEVICE";
    public static final String EXTRA_MESSAGE_COUNT = "com.example.piotrek.bikespeedometer.EXTRA_MESSAGE_COUNT";
    public static final String EXTRA_TEXTVIEW_TO_UPDATE = "com.example.piotrek.bikespeedometer.EXTRA_TEXTVIEW_TO_UPDATE";
    public static TextView dataTextView;
    public static TextView countTextView;
    public static boolean isServiceStarted = false;
}
