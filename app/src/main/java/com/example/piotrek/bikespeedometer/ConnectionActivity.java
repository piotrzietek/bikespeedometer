package com.example.piotrek.bikespeedometer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;


public class ConnectionActivity extends ActionBarActivity {
    private BluetoothAdapter BA;
    private TextView speedometerDataTextView;
    private TextView messageCountTextView;
    private CheckBox connectedCheckBox;
    private boolean deviceFound = false;
    private BluetoothDevice bikeSpeedometer;
    private Intent startConnectionServiceIntent;

    private BroadcastReceiver speedDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.ACTION_SPEED_DATA_RECEIVED)) {
                Bundle extras = intent.getExtras();
                int messageCount = extras.getInt(Constants.EXTRA_MESSAGE_COUNT);
                String speedometerData = extras.getString(Constants.EXTRA_SPEED_DATA);
                speedometerDataTextView.setText(speedometerData);
                messageCountTextView.setText(Integer.toString(messageCount));
                Log.d(Constants.TAG, "Data received...");
            }
        }
    };

    private BroadcastReceiver speedometerFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.ACTION_SPEEDOMETER_FOUND)) {
                if (!connectedCheckBox.isChecked()) {
                    connectedCheckBox.setChecked(true);
                }
            }
            if (action.equals(Constants.ACTION_SPEEDOMETER_DISCONNECTED)) {
                if (connectedCheckBox.isChecked()) {
                    connectedCheckBox.setChecked(false);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(Constants.TAG, "");
        setContentView(R.layout.activity_connection);
        speedometerDataTextView = (TextView)findViewById(R.id.speedometer_data_text_view);
        Constants.dataTextView = speedometerDataTextView;
        messageCountTextView = (TextView)findViewById(R.id.message_count_text_view);
        Constants.countTextView = messageCountTextView;
        connectedCheckBox = (CheckBox)findViewById(R.id.connected_check_box);
        //registerReceiver(speedDataReceiver, new IntentFilter(Constants.ACTION_SPEED_DATA_RECEIVED));
        registerReceiver(speedometerFoundReceiver, new IntentFilter(Constants.ACTION_SPEEDOMETER_FOUND));
        registerReceiver(speedometerFoundReceiver, new IntentFilter(Constants.ACTION_SPEEDOMETER_DISCONNECTED));
        BA = BluetoothAdapter.getDefaultAdapter();
//        bikeSpeedometer = getIntent().getParcelableExtra(Constants.EXTRA_SPEEDOMETER_DEVICE);
//        deviceFound = true;
        Log.d(Constants.TAG, "ConnectionActivity started, device taken from extras");

//        Log.d(Constants.TAG,"Starting ConnectionService");
//        startConnectionServiceIntent = new Intent(ConnectionActivity.this, ConnectionService.class);
//        startConnectionServiceIntent.putExtra(Constants.EXTRA_SPEEDOMETER_DEVICE, bikeSpeedometer);
//        //startConnectionServiceIntent.putExtra(Constants.EXTRA_TEXTVIEW_TO_UPDATE, )
//        startService(startConnectionServiceIntent);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_connection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(startConnectionServiceIntent);
        //unregisterReceiver(speedDataReceiver);
        unregisterReceiver(speedometerFoundReceiver);
    }
}
